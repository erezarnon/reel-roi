#!/usr/bin/env ruby

require 'rubygems'
require 'dbi'
require 'date'
require 'optparse'
require 'fileutils'
require 'net/http'
#require 'net/ssh'
#require 'net/scp'

# set environment variables pointing ODBC to my .ini files
ENV['ODBCINI'] = File.expand_path(File.join(File.dirname(__FILE__)))
ENV['ODBCSYSINI'] = ENV['ODBCINI']
ENV['NZ_ODBC_INI_PATH'] = ENV['ODBCINI']

BEGINNING_OF_TIME = Date.civil(1970, 1, 1)
CURDATE = (Date.today - BEGINNING_OF_TIME).to_i
UP_ARROW = "&#9650;"
DOWN_ARROW = "&#9660;"
FIX_GROUP_CONCAT_SQL = "SET SESSION group_concat_max_len = 1638400"  # Increase if group_concat's get truncated
TEST_EMAILS_TO = ["osexternal@adap.tv"]
INTERNATIONAL_COUNTRIES = "'Bosnia','Macedonia','Ukraine','United Kingdom','Germany','France','Netherlands','Ireland','Italy','Spain','Portugal','Sweden','Norway','Denmark','Finland','Iceland','Turkey','Belgium','Greece','Austria','Switzerland','Poland','Bulgaria','Romania','Serbia','Croatia','Czech Republic','Russian Federation','Hungary','Bulgaria','Slovakia','India','Australia','Canada'"

# corresponds to the PlacementEntityType enum
OVERLAY = 0
MIDROLL = 1
PREROLL = 2
POSTROLL = 3
POSTPLATE = 4
WHOLE_OVERLAY = 5

AD_TYPE = {
    :OO => 0,
    :AD_FOR_PRIVATE_PLACEMENT => 1,
    :AD_FOR_PRIVATE_PLACEMENTS => 2,
    :ATM => 3,
    :RTB => 4,
    :MARKET_PLACEMENT => 5,
    :PRIVATE_PLACEMENT => 6,
    :INSERTION_ORDER => 7,
    :COMMITTED_INVENTORY_SOURCE => 8,
    :RTB_INVENTORY_SOURCE => 9,
    :AD_FOR_EXCHANGES => 10,
    :AD_FOR_MARKETPLACES => 11,
    :AD_FOR_TV => 12
}

#CSHOSTNAME = 'sj-cstest01.sj.adap.tv'
#CSUSERNAME = 'adaptv'
#CSKEY = '/Users/adaptv/.ssh/id_rsa_db'

def percent(numerator, denominator)
  format_number(100 * numerator / denominator.to_f, :percent)
end

def date_number(date)
  (date - BEGINNING_OF_TIME).to_i
end

def stats_date_to_date(stats_date)
  BEGINNING_OF_TIME + stats_date
end

def stats_date_to_unixtimestamp(stats_date)
  stats_date * 86400
end

def format_number(number, format_type)
  prefix = ''
  if number < 0
    prefix = '-'
    number *= -1
  end
  prefix + (case format_type
    when :integer then "%d"
    when :money then "$%.2f"
    when :percent then "%.2f%%"
    when :money_trunc then "$%d"  # We don't care about cents
  end % number).reverse.gsub(%r{([0-9]{3}(?=([0-9])))}, "\\1,").reverse
end

def send_email(from, to, subject, message, attachment=nil, attachment_name=nil)
  attachment_hash = nil
  if attachment
    if (attachment_name.nil?)
      attachment_name = "data.tsv"
    end
    attachment_hash = {attachment_name => attachment}
  end

  send_email_with_attachment_hash(from, to, subject, message, attachment_hash)
end

def send_email_with_attachment_hash(from, to, subject, message, attachment_hash)
  if $test_mode
    to = TEST_EMAILS_TO.join(';')
  elsif $test_to
    to = $test_to
  end
  if attachment_hash
    Pony.mail(:to => to, :from => from, :subject => subject, :html_body => message, :via => :sendmail, :attachments => attachment_hash)
  else
    Pony.mail(:to => to, :from => from, :subject => subject, :html_body => message, :via => :sendmail)
  end
  LOG.info "Sent email to #{to}"
end

# Convert comma-separated list of integers to an actual list of integers, e.g., "1,2,3" => [1, 2, 3]
def deconcat(list)
  if list
    list.split(",").map(&:to_i)
  else
    Array.new
  end
end

def extract_ids(rows)
  rows.map { |row| row['id'] }
end

def using_edds
  DBI.connect("DBI:Mysql:edds:#{EDDS_SERVER}", EDDS_USERNAME, EDDS_PASSWORD) do |edds|
    edds.do FIX_GROUP_CONCAT_SQL
    yield edds
  end
end

def using_config
  DBI.connect("DBI:Mysql:config:localhost", 'flixtrix', 'local') do |config|
    config.do FIX_GROUP_CONCAT_SQL
    yield config
  end
end

def using_dynamic_lookups
  DBI.connect("DBI:Mysql:dynamic_lookups:#{DYNAMIC_LOOKUPS_SERVER}", DYNAMIC_LOOKUPS_USERNAME, DYNAMIC_LOOKUPS_PASSWORD) do |dynamic_lookups|
    dynamic_lookups.do FIX_GROUP_CONCAT_SQL
    yield dynamic_lookups
  end
end

def using_audience_data
  DBI.connect("DBI:Mysql:audience_data:#{AUDIENCE_DATA_SERVER}", AUDIENCE_DATA_USERNAME, AUDIENCE_DATA_PASSWORD) do |audience_data|
    audience_data.do FIX_GROUP_CONCAT_SQL
    yield audience_data
  end
end

def using_bonus_reporting
  DBI.connect("DBI:Mysql:bonus_reporting:#{BONUS_REPORTING_SERVER}", BONUS_REPORTING_USERNAME, BONUS_REPORTING_PASSWORD) do |bonus_reporting|
    bonus_reporting.do FIX_GROUP_CONCAT_SQL
    yield bonus_reporting
  end
end

def using_log
  DBI.connect("DBI:Mysql:log:#{LOG_SERVER}", LOG_USERNAME, LOG_PASSWORD) do |log|
    log.do FIX_GROUP_CONCAT_SQL
    yield log
  end
end

def using_reporting
  DBI.connect("DBI:ODBC:NJNZSQL", 'erez', 'dyq4o83QEQ!') do |netezza_reporting|
    yield netezza_reporting
  end
end

def using_stats_summary
  DBI.connect("DBI:Mysql:stats_summary:#{STATS_SUMMARY_SERVER}", STATS_SUMMARY_USERNAME, STATS_SUMMARY_PASSWORD) do |stats_summary|
    yield stats_summary
  end
end

def using_billing
  DBI.connect("DBI:Mysql:billing:#{BILLING_SERVER}", BILLING_USERNAME, BILLING_PASSWORD) do |billing|
    yield billing
  end
end

def using_summary_meta_data
  DBI.connect("DBI:Mysql:stats_summary_metadata:#{SUMMARY_META_DATA_SERVER}", SUMMARY_META_DATA_USERNAME, SUMMARY_META_DATA_PASSWORD) do |summary_meta_data|
    yield summary_meta_data
  end
end

def using_reporting_lookups
  DBI.connect("DBI:Mysql:reporting_lookups:#{REPORTING_LOOKUP_SERVER}", RL_USER, RL_PASSWORD) do |reporting_lookups|
    reporting_lookups.do FIX_GROUP_CONCAT_SQL
    yield reporting_lookups
  end
end

def using_test_billing
  DBI.connect("DBI:Mysql:#{BILLING_TEST_DB}:#{BILLING_TEST_SERVER}", BILLING_TEST_USERNAME, BILLING_TEST_PASSWORD) do |billing|
    yield billing
  end
end

TEST_ORGS = using_config do |config|
    config.select_one("SELECT id from organizations where account_type = 'TEST' or company_name like '%test%'")
end

AND_AD_SOURCE_NOT_IN_TEST_ORGS  = TEST_ORGS.nil? ? "" : "AND a.organization_id NOT IN (#{TEST_ORGS.join(',')})"

def oo_ads
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(a.id) ids FROM ad_sources a where a.ad_type = 'OO'")['ids']
  end
end

def market_sell_ads
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(a.id) ids FROM ad_sources a where a.ad_type = 'MARKET_PLACEMENT'")['ids']
  end
end

def self_service_orgs
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(o.id) ids FROM organizations o where o.account_type = 'SELF_SERVICE'")['ids']
  end
end

def managed_orgs
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(o.id) ids FROM organizations o where o.account_type = 'MANAGED'")['ids']
  end
end

def orgs_with_capped_spend_pricing_model
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(o.id) ids FROM organizations o where o.account_type='MANAGED' and o.managed_pricing_model = 'CAPPED_TOTAL_AD_SPEND_ECPM'")['ids']
  end
end

def orgs_with_fixed_io_pricing_model
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(o.id) ids FROM organizations o where o.account_type='MANAGED' and o.managed_pricing_model = 'FIXED_IO_PRICE'")['ids']
  end
end

def market_buy_ads
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(a.id) ids FROM ad_sources a where a.ad_type IN ('ATM', 'RTB')
    #{AND_AD_SOURCE_NOT_IN_TEST_ORGS}")['ids']
  end
end

def market_buy_ads_except_third_party_tracked_ios
  adaptv_tracked_io_campaigns = using_billing do |billing|
    extract_ids(billing.select_all("SELECT distinct campaign_id id FROM io_lines i
    WHERE i.third_party_tracking_id = (SELECT id from third_party_trackings where external_id='ADAPTV')")).join(',')
  end

  or_campaign_is_adaptv_tracked_io = adaptv_tracked_io_campaigns.nil? ? "" : "OR a.campaign_id IN (#{adaptv_tracked_io_campaigns})"

  using_config do |config|
    extract_ids(config.select_all("SELECT a.id FROM ad_sources a WHERE a.ad_type IN ('ATM', 'RTB')
    AND (a.campaign_id IN (SELECT c.id FROM campaigns c WHERE c.uses_io = 0) #{or_campaign_is_adaptv_tracked_io})
    #{AND_AD_SOURCE_NOT_IN_TEST_ORGS}"))
  end
end

def market_buy_ads_for_third_party_tracked_ios
  third_party_tracked_io_campaigns = using_billing do |billing|
    extract_ids(billing.select_all("SELECT distinct campaign_id id FROM io_lines i
    WHERE i.third_party_tracking_id != (SELECT id FROM third_party_trackings WHERE external_id = 'ADAPTV')")).join(',')
  end

  and_campaign_is_third_party_tracked_io = third_party_tracked_io_campaigns.nil? ? "" : "AND a.campaign_id IN (#{third_party_tracked_io_campaigns})"

  using_config do |config|
    extract_ids(config.select_all("SELECT a.id FROM ad_sources a WHERE a.ad_type IN ('ATM', 'RTB')
    #{and_campaign_is_third_party_tracked_io}
    #{AND_AD_SOURCE_NOT_IN_TEST_ORGS}"))
  end
end

def market_buy_ads_for_self_service_orgs
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(a.id) ids FROM ad_sources a where a.ad_type IN ('ATM', 'RTB')
      and a.organization_id IN (SELECT o.id FROM organizations o where o.account_type = 'SELF_SERVICE')")['ids']
  end
end

def market_buy_ads_for_platform_users
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(a.id) ids FROM ad_sources a where a.ad_type IN ('ATM', 'RTB')
      and a.organization_id IN (SELECT o.id FROM organizations o where o.has_platform_fee = 1 and o.account_type != 'TEST')")['ids']
  end
end

def market_buy_ads_for_managed_orgs
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(a.id) ids FROM ad_sources a where a.ad_type IN ('ATM', 'RTB')
      and a.organization_id IN (SELECT o.id FROM organizations o where o.account_type = 'MANAGED')")['ids']
  end
end

def market_buy_ads_for_capped_ad_spend_managed_orgs
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(a.id) ids FROM ad_sources a where a.ad_type IN ('ATM', 'RTB')
      and a.organization_id IN (SELECT o.id FROM organizations o where o.account_type = 'MANAGED' and o.managed_pricing_model = 'CAPPED_TOTAL_AD_SPEND_ECPM')")['ids']
  end
end

def non_adaptv_clearing_house_market_places
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(DISTINCT m.id) ids FROM market_places m where m.reporting_type = 'PRIVATE_MARKETPLACE' OR (m.reporting_type = 'EXTERNAL_EXCHANGE' AND m.is_adaptv_clearing_house=0)")['ids']
  end
end

def adaptv_cleared_market_places
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(DISTINCT m.id) ids FROM market_places m where m.id NOT IN (SELECT mrktplaces.id from market_places mrktplaces where mrktplaces.reporting_type = 'PRIVATE_MARKETPLACE' OR (mrktplaces.reporting_type = 'EXTERNAL_EXCHANGE' AND mrktplaces.is_adaptv_clearing_house=0))")['ids']
  end
end

def market_buyers
  using_config do |config|
    deconcat config.select_one("select group_concat(organization_id) ids from (select organization_id, group_concat(attribute_type) attributes from organization_attributes group by organization_id having attributes like '\%MARKET_BUYER\%') x")['ids']
  end
end

def market_sellers
  using_config do |config|
    deconcat config.select_one("select group_concat(organization_id) ids from (select organization_id, group_concat(attribute_type) attributes from organization_attributes group by organization_id having attributes like '\%MARKET_SELLER\%') x")['ids']
  end
end

def second_price_ads
  using_config do |config|
    deconcat config.select_one("SELECT GROUP_CONCAT(id) ids FROM ad_sources WHERE organization_id IN (1687, 2078, 2257, 2170) OR id IN (8698, 8699, 10948, 10949, 10950)")['ids']
  end
end

def apply_pricing_override(ad_source_id, price)
  case ad_source_id
    when 9608, 9609 then 8.3
    when 10254, 10255, 10292, 10293, 10295 then 12.0
    when 10769, 10770 then 7
    when 10644 then 7.64
    when 11262 then 8
    else price
  end
end

def special_pricing_ads
  return [9608, 9609, 10254, 10255, 10292, 10293, 10295, 10769, 10770, 10644, 11262]
end

def international_country_ids
  using_config do |config|
    deconcat config.select_one("SELECT GROUP_CONCAT(id) ids FROM human_locations WHERE human_name in (#{INTERNATIONAL_COUNTRIES}) and location_type = 1")['ids']
  end
end

def excluded_publishers
  using_config do |config|
    deconcat config.select_one("SELECT group_concat(id) ids from organizations where left(external_id,6)='adaptv' or right(external_id,5) = '_test' or external_id = 'hj438'")['ids']
  end
end

def excluded_publisher_ids
  using_config do |config|
    config.select_one("SELECT group_concat(id) exclude_publisher_ids from organizations where left(external_id,6)='adaptv' or right(external_id,5) = '_test' or external_id = 'hj438'")['exclude_publisher_ids']
  end
end

def ato_publishers
  using_config do |config|
    deconcat config.select_one("select group_concat(organization_id) ids from (select organization_id, group_concat(attribute_type) attributes from organization_attributes group by organization_id having attributes like '\%AD_PLAYER_SELLER\%') x")['ids']
  end
end

def atm_publishers
  using_config do |config|
    deconcat config.select_one("select group_concat(organization_id) ids from (select organization_id, group_concat(attribute_type) attributes from organization_attributes group by organization_id having attributes like '\%MARKET_SELLER\%' and attributes not like '\%AD_PLAYER_SELLER\%') x")['ids']
  end
end

def two_dimensional_counter
  Hash.new { |hash, key| hash[key] = Hash.new { |h, k| h[k] = 0 } }
end

class Array
  def sum
    inject(0) do |s, x|
      s + x
    end
  end
end

def deep_copy(obj)
  Marshal.load(Marshal.dump(obj))
end

class Hash
  def hash_map
    self.inject({}) do |new_hash, (key, val)|
      tuple = yield(key, val)
      new_hash[tuple[0]] = tuple[1]
      new_hash
    end
  end
end

def audience_data_shortribs_mgetn(keys)
    return shortribs_mgetn(AUDIENCE_DATA_SHORTRIBS_SERVER + ":" + AUDIENCE_DATA_SHORTRIBS_PORT, keys)
end

def shortribs_mgetn(url_base, keys)
    uri = URI.parse("http://" + url_base + "/mgetn/" + keys.join(','))
    http = Net::HTTP.new(uri.host,uri.port)
    http.open_timeout = http.read_timeout = 300
    response = http.request(Net::HTTP::Get.new(uri.request_uri))
    ret_map = {}
    response.body.split("\n").each do |row|
        key, value = row.split("\t")
        ret_map[key] = value
    end
    return ret_map
end

def is_float?(str)
  true if Float(str) rescue false
end

def is_int?(str)
  true if Integer(str) rescue false
end
