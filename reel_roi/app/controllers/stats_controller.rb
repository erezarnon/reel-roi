require 'adaptv'
require 'net/http'
require 'json'
require "open-uri"

class StatsController < ApplicationController
  helper_method :index
  
  START_STATS_DATE = 16290
  START_TIME = stats_date_to_unixtimestamp(START_STATS_DATE)
  END_STATS_DATE = 16290
  END_TIME = stats_date_to_unixtimestamp(END_STATS_DATE)
  ORG_ID = 7299
  SIGNIFICANCE_THRESHOLD = 1000
  NUM_MARKETPLACE_LOGOS = 5
  MAX_FREQUENCY = 15
  
  def query_google_images(query)
    params = {'v'=> 1.0, 'q' => query}
    return JSON.parse(Net::HTTP.get('ajax.googleapis.com', "/ajax/services/search/images?".concat(params.collect { |k,v| "#{k}=#{CGI::escape(v.to_s)}" }.join('&'))))
  end
  
  def get_logo(advertiser_name)
    json = query_google_images(advertiser_name)
    return json['responseData']['results'][0]['unescapedUrl']
  end
  
  def get_team_pics(config)
    pics = []
    config.select_all("SELECT company_name, a.first_name, a.last_name FROM users a INNER JOIN organizations b ON a.organization_id = b.id  WHERE a.organization_id = #{ORG_ID} AND a.acct_enabled = 1") do |row|
      json = query_google_images("#{row['company_name'].split(' ')[0]} #{row['first_name']} #{row['last_name']}")
      best_pic = json['responseData']['results'][0]
      max_squareness = 1000000
      json['responseData']['results'][0..5].each do |pic|
        squareness = pic['tbWidth'].to_f / pic['tbHeight'].to_f
        squareness = 1 / squareness if squareness < 1
        if squareness < max_squareness
          max_squareness = squareness
          best_pic = pic
        end
      end

      pics << {
        'first_name' => row['first_name'],
        'last_name' => row['last_name'],
        'pic' => best_pic['unescapedUrl']
      }
    end
    
    return pics
  end
    
  def get_creatives(config)
    sth = config.execute("SELECT mf.id, mf.name, mf.url, mf.format_type, mf.status, mf.delivery,
      mf.width, mf.height, mf.bitrate_kbps, IFNULL(cmf.creative_id, 0) AS creative_id,
      IFNULL(mf.campaign_id, 0) AS campaign_id, IFNULL(mf.transcoding_template_id, 0) AS template_id,
      IFNULL(mf.source_url, '') AS source_url FROM media_files mf LEFT JOIN creatives_media_files cmf ON mf.id = cmf.media_file_id WHERE TRUE  AND format_type NOT IN ('GIF', 'PNG', 'JPEG') AND cmf.creative_id IN (SELECT id FROM creatives WHERE organization_id = #{ORG_ID} AND status IN (0,1))")

    media_files = []
    while row = sth.fetch_hash
      media_files << row
    end
    
    return media_files
  end
  
  def get_best_performing_creative(reporting, creatives)
    creatives_completion_rates = reporting.select_all("SELECT creative_id \"creative_id\", SUM(completions100) \"completions\", SUM(ad_successes) \"impressions\" FROM vw_smry_mrkts WHERE manager_id = #{ORG_ID} AND stats_date >= #{START_STATS_DATE} AND stats_date <= #{END_STATS_DATE} GROUP BY creative_id")
    best_performing_creative_id = creatives_completion_rates.max_by do |row|
      if row['completions'] > SIGNIFICANCE_THRESHOLD and row['impressions'] > SIGNIFICANCE_THRESHOLD
        row['completions'] / row['impressions'].to_f
      else
        -1
      end
    end['creative_id']

    creatives.each do |creative|
      if creative['creative_id'].to_i == best_performing_creative_id.to_i
        return creative
      end          
    end
  end
  
  def get_completion_rates_by_frequency(reporting)
    completion_rates_by_frequency = []
    reporting.select_all("SELECT impressions \"frequency\", SUM(impressions) \"impressions\", SUM(completions) \"completions\" FROM
      (SELECT SUM(completions100) completions, SUM(ad_successes) impressions FROM vw_unique_stats
      WHERE organization_id = #{ORG_ID} AND stats_date >= #{START_STATS_DATE} AND stats_date <= #{END_STATS_DATE} GROUP BY uid, ad_source_id) smry
      GROUP BY impressions") do |row|
        completion_rates_by_frequency[row['frequency'].to_i] = row['impressions'].to_f > 0 ? row['completions'].to_f / row['impressions'].to_f : 0
      end
    completion_rates_by_frequency.take(MAX_FREQUENCY)[1..-1]
  end
  
  module SEGMENT_TYPES
    FIRST_PARTY = 1
    THIRD_PARTY = 2
  end
  
  def get_segment_data(config, reporting, targeting_expressions_by_ad, ads_to_tactics, response)
    audience_data_providers = []
    sth = config.execute("SELECT id, display_name, segment_display_name, logo_url FROM audience_data_providers")
    while row = sth.fetch_hash
      audience_data_providers << row
    end
    audience_data_providers_by_segment_display_name = {}
    audience_data_providers_by_id = {}
    audience_data_providers.each do |provider|
      audience_data_providers_by_segment_display_name[provider['segment_display_name']] = provider
      audience_data_providers_by_id[provider['id'].to_i] = provider
    end
    
    segments_targeted = []
    targeting_expressions_by_ad.each do |ad, expression|
      expression.scan(/\[(.*?)\] IN \((.*?\")\)/).each do |match|
        unless audience_data_providers_by_segment_display_name[match[0]].nil?
          match[1].scan(/\d+/).each { |id| segments_targeted << id }
          ads_to_tactics[ad] << :BT
        end
      end
    end
    segments_targeted = segments_targeted.map { |id| id.to_i }.uniq
    
    segments_reached = reporting.select_all("SELECT DISTINCT segment_id \"segment_id\" FROM vw_user_segments_summary WHERE manager_id = #{ORG_ID} AND stats_date >= #{START_STATS_DATE} AND stats_date <= #{END_STATS_DATE}").map do |row|
      row['segment_id'].to_i
    end 
    targeted_segments_reached = segments_targeted & segments_reached

    return 0, 0 if 0 == targeted_segments_reached.length
    
    first_party_targeted_segments_reached = []
    third_party_targeted_segments_reached = []
    logos = []
    config.select_all("SELECT id, provider_id, CASE WHEN data_fee_cpm = 0 THEN #{SEGMENT_TYPES::FIRST_PARTY} ELSE #{SEGMENT_TYPES::THIRD_PARTY} END AS type FROM audience_data_provider_segments WHERE id IN (#{targeted_segments_reached.join(',')})") do |row|
      if SEGMENT_TYPES::FIRST_PARTY == row['type']
        first_party_targeted_segments_reached << row['id']
      else
        third_party_targeted_segments_reached << row['id']
      end
      logos << audience_data_providers_by_id[row['provider_id'].to_i]['logo_url']
    end

    uniques_reached = reporting.select_one("SELECT COUNT(DISTINCT u.uid) \"uniques\" FROM vw_unique_stats u JOIN
      (SELECT DISTINCT uid, stats_date FROM vw_unique_user_segments WHERE stats_date >= #{START_STATS_DATE} AND stats_date <= #{END_STATS_DATE} AND segment_id IN (#{first_party_targeted_segments_reached.join(',')})) s
      ON u.uid = s.uid AND u.stats_date = s.stats_date WHERE organization_id = #{ORG_ID} AND u.stats_date >= #{START_STATS_DATE} AND u.stats_date <= #{END_STATS_DATE}")['uniques']
          
    response['first_party_targeted_segments_activated'] = first_party_targeted_segments_reached.length
    response['third_party_targeted_segments_activated'] = third_party_targeted_segments_reached.length
    response['uniques_reached_using_first_party_data'] = uniques_reached.to_i  
    response['audience_data_provider_logos'] = logos.uniq
  end
  
  module MARKET_TYPES
    PRIVATE = 1
    OPEN = 2
    end

  def get_marketplace_data(marketplaces, config)
    private_marketplaces = []
    open_exchanges = []
    ids = marketplaces.map { |market| market['id'] }
    config.select_all("SELECT id, console_header_logo_url logo_url, organization_id, CASE WHEN reporting_type = 'PRIVATE_MARKETPLACE' THEN #{MARKET_TYPES::PRIVATE} ELSE #{MARKET_TYPES::OPEN} END AS type FROM market_places WHERE id IN (#{ids.join(',')})") do |row|
      if MARKET_TYPES::PRIVATE == row['type']
        private_marketplaces << row
      else
        open_exchanges << row
      end
    end
    
#    top_private_marketplace_logos = private_marketplaces.sort { |market| market['id'] }.last(NUM_MARKETPLACE_LOGOS).map { |market| market['logo_url'] }
    top_open_exchange_logos = open_exchanges.sort { |market| market['id'] }.last(NUM_MARKETPLACE_LOGOS).map { |market| market['logo_url'] }
    top_private_marketplace_org_ids = private_marketplaces.sort { |market| market['id'] }.last(NUM_MARKETPLACE_LOGOS).map { |market| market['organization_id'] }
    top_private_marketplace_logos = config.select_all("SELECT company_name FROM organizations where id IN (#{top_private_marketplace_org_ids.join(',')})").map do |row|
      get_logo("\"#{row['company_name'].split(' ')[0]} logo\"")
    end
    return private_marketplaces.length, top_private_marketplace_logos, open_exchanges.length, top_open_exchange_logos
  end
    
  def get_costs(config, ad_data, response)
    login_info = JSON.parse(URI.parse("https://go.adap.tv:443/sessions/login?un=erez@adap.tv&pw=dyq4o83QEQ!").read)
    session =  login_info['session_id']
    uri = "https://go.adap.tv/reporting/run_report?org_id=#{ORG_ID}&keys=month,ad_id&metrics=ad_impressions,media_spend,ata_media_cost,cost&start_date=#{START_TIME}&end_date=#{END_TIME}&_sid=#{session}"
    
    data = JSON.parse(URI.parse(uri).read)
    response['total_ad_spend'] = {}
    response['media_cost_savings'] = 0
    data['data'].each do |row|
      response['total_ad_spend'][row['row'][0]] = 0 if response['total_ad_spend'][row['row'][0]].nil?
      media_cost = row['row'][3].to_f + row['row'][4].to_f
      response['total_ad_spend'][row['row'][0]] += row['row'][5].to_f
      would_have_spent = ad_data[row['row'][1].to_i]['cost'] * row['row'][2].to_i / 1000.0
      response['media_cost_savings'] += would_have_spent - media_cost
    end
  end

  module TACTICS
    MASS_REACH = 1
    SITE_LIST = 2
    MOBILE = 3
    BT = 4
  end
  
  def get_tactic_data(config, ad_data, ads_to_tactics, response)
    config.select_all("SELECT ad.id, site_targeting_entity_string_value FROM ad_sources ad JOIN content_descriptions cd ON ad.id=cd.ad_source_id
      JOIN site_targeting_entities ste ON ste.content_description_id=cd.id
      JOIN site_targeting_entity_values stev ON stev.site_targeting_entity_id=ste.id WHERE ad.organization_id = #{ORG_ID}") do |row|
      ads_to_tactics[row['id'].to_i] << :MOBILE if row['site_targeting_entity_string_value'].include?('MOBILE')
    end
    
    config.select_all("SELECT ad.id FROM ad_sources ad JOIN domain_custom_targeting_lists tl ON ad.id=tl.ad_source_id WHERE ad.organization_id = #{ORG_ID}") do |row|
      ads_to_tactics[row['id'].to_i] << :SITE_LIST
    end
    
    ads_to_tactics.keys.each do |ad|
      ads_to_tactics[ad].uniq!
      ads_to_tactics[ad] << :MASS_REACH unless ads_to_tactics[ad].include?(:BT) or ads_to_tactics[ad].include?(:SITE_LIST)
    end
    
    response[:tactics] = TACTICS.constants.inject({}) do |hsh, tactic|
      hsh[tactic] = { :impressions => 0, :completions => 0 }
      hsh
    end
    
    ad_data.each do |ad, data|
      ads_to_tactics[ad].each do |tactic|
        response[:tactics][tactic][:impressions] += data[:impressions]
        response[:tactics][tactic][:completions] += data[:completions]
      end
    end
    
    response[:tactics].each do |tactic, stats|
      stats[:completion_rate] = stats[:impressions] > 0 ? stats[:completions].to_f / stats[:impressions].to_f : 0
    end
  end
      
  def index
    response = {}
    ads_to_max_bids = {}
    using_config do |config|
      row = config.select_one("SELECT COUNT(*) AS count, MIN(UNIX_TIMESTAMP(created_at)) AS first_campaign_date FROM campaigns WHERE organization_id = #{ORG_ID}")
      response['first_campaign_date'] = row['first_campaign_date']
      response['num_campaigns'] = row['count']
        
      row = config.select_one("SELECT UNIX_TIMESTAMP(created_at) created_at, company_name FROM organizations WHERE id = #{ORG_ID}")
      response['org_join_date'] = row['created_at']
      
      response['logo'] = get_logo(row['company_name'])
      response['team'] = get_team_pics(config)
      
      creatives = get_creatives(config)
      response['first_creative'] = creatives.min_by { |creative| creative['creative_id'] }
      response['media_files'] = creatives.uniq { |creative| creative['campaign_id'] }
      
      using_reporting do |reporting|
        ad_data = {}
        targeting_expressions_by_ad = {}
        ads_to_tactics = {}
        sth = config.execute("SELECT id, cost, targeting_expression FROM ad_sources WHERE organization_id = #{ORG_ID}")
        
        while row = sth.fetch_hash
          ad_data[row['id'].to_i] = {}
          data = ad_data[row['id'].to_i]
          data['id'] = row['id'].to_i
          data['cost'] = row['cost'].to_f
          data[:impressions] = 0
          data[:completions] = 0
          ads_to_tactics[row['id'].to_i] = []  
          targeting_expressions_by_ad[row['id'].to_i] = row['targeting_expression'] unless row['targeting_expression'].nil?
        end
        
        response[:impressions] = response[:completions] = 0
        marketplaces = []
        inventory_sources = []
        response[:impressions] = response[:completions] = 0
        reporting.select_all("SELECT marketplace_id \"marketplace_id\", private_placement_id \"private_placement_id\", ad_source_id \"ad_id\", SUM(ad_successes) \"impressions\", SUM(completions100) \"completions\"
          FROM vw_smry_daily WHERE manager_id = #{ORG_ID} AND stats_date >= #{START_STATS_DATE} AND stats_date <= #{END_STATS_DATE} GROUP BY marketplace_id, private_placement_id, ad_source_id
          HAVING SUM(ad_successes) + SUM(completions100) > 0") do |row|
          response[:impressions] += row['impressions'].to_i
          response[:completions] += row['completions'].to_i
          marketplaces << {'id' => row['marketplace_id'].to_i, :impressions => row[:impressions].to_i }
          inventory_sources << row['private_placement_id'].to_i if [2, 3].include?(row['ad_type'].to_i)
          ad_data[row['ad_id'].to_i][:impressions] += row['impressions'].to_i
          ad_data[row['ad_id'].to_i][:completions] += row['completions'].to_i
        end
        response['private_marketplaces'], response['private_marketplace_logos'], response['open_exchanges'], response['open_exchange_logos'] = get_marketplace_data(marketplaces, config)
        response['upfronts'] = inventory_sources.uniq.length
        response['best_performing_creative'] = get_best_performing_creative(reporting, creatives)
        response['completion_rates_by_frequency'] = get_completion_rates_by_frequency(reporting)
        response['recommended_frequency'] = response['completion_rates_by_frequency'].each_with_index.max[1]
  
        get_segment_data(config, reporting, targeting_expressions_by_ad, ads_to_tactics, response)
        get_tactic_data(config, ad_data, ads_to_tactics, response) 
        get_costs(config, ad_data, response)
      end

    end
      
      render :json => response
  end
end
